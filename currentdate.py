from datetime import datetime

def get_current_time():
    # Get the current local date and time
    current_time = datetime.now()

    # Format the current time as a string
    formatted_time = current_time.strftime("%Y-%m-%d %H:%M:%S")

    return formatted_time

# Get and print the current time
current_time = get_current_time()
print("Current time on Windows PC:", current_time)

